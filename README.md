# Victoria
Simple custom firewall used for the games

- [Requirements](#requirements)
  - [System](#system)
  - [Packages](requirements.txt)
- [Setup](#setup)
- [Start](#start)
- [Contributions](#contributions)
- [Changelog](CHANGELOG.md)
- [License](LICENSE)

## Requirements

#### System
- Python 3.8+ 64 bit
- Windows Vista/7/8/10 or Windows Server 2008 64 bit
- Administrator Privileges

## Setup
1. Clone this repo somewhere you want to "save it"

#### Automatic
run `setup.bat` as admin

#### (or) Manual
1. Install python and pip See [System](#system) for version.

   If you are unsure where to find this you can just download this [https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64.exe](https://www.python.org/ftp/python/3.8.0/python-3.8.0-amd64.exe)
   > Remember to check the add to system environment checkbox.
                                                                                                                                                                                                                                                            
2. Install virtualenv
   ```text
   pip install virtualenv
   ```

3. Go to where you cloned this repo and start CMD from there
   ```text
   virtualenv venv
   venv\Scripts\activate
   ```
4. Install requirements
   ```text
   pip install -r requirements.txt
   ```
## Start 

#### Automatic
run `victoria.bat` as admin

#### (or) Manual
1. Go to where you cloned this repo and start CMD from there
   ```text
   venv\Scripts\activate
   ```
2. Start
   ```text
   python app.py
   ```

## Contributions
All contributions are helpful, feel free to make a Merge Request.